import React, { Fragment, useState } from "react";
import { Route } from "react-router-dom";
import Header from "../Layout/Header";
import Home from "../views/Home";
import About from "../views/About";
import Sign from "../views/Sign";
import Footer from "../Layout/Footer";

const Routes = () => {
    return (
        <div>
            <Header />
            <Route path="/" component={Home} exact />
            <Route path="/about" component={About} exact />
            <Route path="/sign" component={Sign} exact />
            <Footer />
        </div>
    ) //Header & Footer ada terus d setiap page jd tdk usah di ksh Route
}
export default Routes;