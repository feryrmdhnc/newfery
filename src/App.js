import React, { Fragment, useState } from "react";
import { BrowserRouter } from "react-router-dom";
import Routes from "./route/Routes";


class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Routes />
      </BrowserRouter>
    )
  }
}

export default App;
